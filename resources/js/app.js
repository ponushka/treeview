
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

window.Vue = require('vue');
import VueRouter from 'vue-router';
window.Vue.use(VueRouter);

import App from './components/App.vue';
window.Vue.component('TreeView', require('./components/TreeView.vue'));
import IndexComponent from './components/IndexComponent.vue';

const routes = [
    {
        path: '/',
        components: {
            index: IndexComponent
        }
    },
];

const router = new VueRouter({ mode: 'history', routes: routes});

new window.Vue(window.Vue.util.extend({ router }, App)).$mount('#app');
