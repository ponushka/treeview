<?php

namespace App\Http\Controllers;

use App\TreeItem;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TreeItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return array
     */
    public function index()
    {
        $treeViewItems = TreeItem::all();

        return TreeItem::buildTree($treeViewItems);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return TreeItem
     */
    public function show($id)
    {
        return  TreeItem::findOrFail($id);
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function saveTree(Request $request)
    {
        $data = $request->all();
        $idRefs = []; //массив соответствия сгенированных фронтом id и реальных id из базы
        $idsToDelete = []; //массив id элементов, которые будут удалены из базы
        $deleteFromTreeIds = []; //массив id элементов, которые надо будет удалить из локального дерева
        $itemsToHandle = []; //итоговый массив элементов, которым надо заменить id и/или удалить из дерева
        foreach ($data as $item) {
            $addIdToDeleted = false;
            $tempId = $item['id'];

            if ($tempId < 0) {
                $treeItem = new TreeItem();
                //добавленный элемент не может идти первым, так что $idRefs гарантированно будет содержать родителя
                $treeItem->parent_id = $idRefs[$item['parent_id']];
            } else {
                $treeItem = TreeItem::find($tempId);
            }

            $treeItem->name = $item['name'];

            //пометим на удаление ещё не удаленный элемент
            if ($item['deleted'] && !$treeItem->deleted) {
                $treeItem->deleted = $item['deleted'];
                $addIdToDeleted = true;
            }

            $treeItem->save();

            if ($addIdToDeleted) {
                $idsToDelete[] = $treeItem->id;
            }

            $idRefs[$tempId] = $treeItem->id;
        }
        unset($item);

        $deleteFromTreeIds = $idsToDelete;

        //удалим в базе всех потомков удаленных родителей
        while(!empty($idsToDelete)) {
            /* @var \Illuminate\Support\Collection $children */
            $children = TreeItem::whereIn('parent_id', $idsToDelete)->get();
            $idsToDelete = $children->pluck('id')->toArray();
            $deleteFromTreeIds = array_merge($deleteFromTreeIds, $idsToDelete);
            $children->each(function(TreeItem $child) {
               $child->deleted = true;
               $child->save();
            });
        }

        //найдем пересечение всех удаленных элементов с элементами, которые надо будет обработать в локальном дереве
        $deleteFromTreeIds = collect($deleteFromTreeIds)->intersect($idRefs)->flip()->toArray();

        foreach($idRefs as $key => $value) {
            $itemsToHandle[] = [
                'oldId'   => $key,
                'newId'   => $value,
                'deleted' => array_key_exists($value, $deleteFromTreeIds),
            ];
        }

        return [
            'tree'          => $this->index(),
            'itemsToHandle' => $itemsToHandle,
        ];
    }

    /**
     * @return array
     */
    public function reset()
    {
        TreeItem::whereInitial(false)->delete();
        TreeItem::all()->each(function(TreeItem $treeItem) {
            $treeItem->name = 'Node' . $treeItem->id;
            $treeItem->deleted = false;
            $treeItem->save();
        });

        return $this->index();
    }
}
