<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\TreeItem
 *
 * @property int $id
 * @property string $name
 * @property int $parent_id
 * @property int $deleted
 * @property int $initial
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TreeItem whereDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TreeItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TreeItem whereInitial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TreeItem whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TreeItem whereParentId($value)
 * @mixin \Eloquent
 */
class TreeItem extends Model
{
    public $timestamps = false;

    /**
     * @param array $sections
     * @param int $parentId
     *
     * @return array
     */
    public static function buildTree(&$sections, $parentId = 0)
    {
        $tree = [];

        foreach ($sections as &$section) {
            if ($section['parent_id'] == $parentId) {
                $children = self::buildTree($sections, $section['id']);
                $section['children'] = $children ?: [];
                array_push($tree, $section);
                unset($section);
            }
        }

        return $tree;
    }
}
