<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/v1', 'as' => 'api.'], function () {
    Route::get('tree-items/reset', 'TreeItemController@reset');
    Route::post('tree-items/save-tree', 'TreeItemController@saveTree');
    Route::resource('tree-items', 'TreeItemController', ['only' => ['index', 'show']]);
});
